# Game Day Suds

## _Shane Stafford, developer_

## Description
This Jetpack Compose app allows you to customize the theme to your favorite MLB team before finding game day suds recommendations. Play ball!

## Getting Started
### Technologies Used
* [Android SDK 23+](https://developer.android.com/docs)
* [Kotlin](https://kotlinlang.org/docs/home.html)
* [Jetpack Compose](https://developer.android.com/jetpack/compose)

### Development Prerequisites
* [Git](https://git-scm.com/)
* [Android Studio](https://developer.android.com/studio/index.html)

### Installation
* `git clone https://gitlab.com/darkknightsds/game-day-suds`
* `cd game-day-suds`

### Running the App
* Open Android Studio
* Choose "Open existing project"
* Select `game-day-suds` directory
* Sync Gradle/ build project
* Press green arrow ("Run") to install APK

## Next Steps
* Clean up and organize the UI components
* Add unit tests for the views

## Background 
I used this project to explore Jetpack Compose, which is something we are just starting to implement at my current job, and has piqued my interest.

## Licensing
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
