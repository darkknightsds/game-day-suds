package com.darkknightsds.gamedaysuds

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.darkknightsds.gamedaysuds.selectTeam.TeamViewModel
import com.darkknightsds.gamedaysuds.selectTeam.TeamBody
import com.darkknightsds.gamedaysuds.suds.*

class MainActivity : ComponentActivity() {
    private val sudsViewModel by viewModels<SudsViewModel>()
    private val teamViewModel by viewModels<TeamViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val navController = rememberNavController()
            SudsNavHost(navController, sudsViewModel, teamViewModel)
        }
    }
}

@Composable
fun SudsNavHost(
    navController: NavHostController,
    sudsViewModel: SudsViewModel,
    teamViewModel: TeamViewModel
) {
    NavHost(
        navController = navController,
        startDestination = ScreenNavigationTypes.SelectTeam.route
    ) {
        composable(route = ScreenNavigationTypes.SelectTeam.route) {
            TeamBody(
                onClickFavoriteTeam = {
                    teamViewModel.selectedTeam = it
                    navController.navigate(ScreenNavigationTypes.Suds.route)
                                      },
                viewModel = teamViewModel
            )
        }
        composable(route = ScreenNavigationTypes.Suds.route) {
            SudsBody(
                onClickBeer = { id ->
                    sudsViewModel.selectedBeer = sudsViewModel.sudsListResponse.firstOrNull { it.id == id }!!
                              },
                viewModel = sudsViewModel,
                selectedTeam = teamViewModel.selectedTeam
            )
        }
    }
}

enum class ScreenNavigationTypes(val route: String) {
    SelectTeam("SelectTeam"),
    Suds("Suds")
}