package com.darkknightsds.gamedaysuds.suds

data class Beer(val id: Int, val name: String, val abv: Float, val ibu: Float, val food_pairing: List<String>)