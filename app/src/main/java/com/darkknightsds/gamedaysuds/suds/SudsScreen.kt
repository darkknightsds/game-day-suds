package com.darkknightsds.gamedaysuds.suds

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.darkknightsds.gamedaysuds.selectTeam.Team
import com.darkknightsds.gamedaysuds.ui.theme.GameDaySudsTheme
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SudsBody(
    onClickBeer: (Int) -> Unit = {},
    viewModel: SudsViewModel,
    selectedTeam: Team
) {
    viewModel.getSuds()
    val scope = rememberCoroutineScope()
    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
        bottomSheetState = rememberBottomSheetState(initialValue = BottomSheetValue.Collapsed)
    )

    val sheetToggle: () -> Unit = {
        scope.launch {
            if (bottomSheetScaffoldState.bottomSheetState.isCollapsed) {
                bottomSheetScaffoldState.bottomSheetState.expand()
            } else {
                bottomSheetScaffoldState.bottomSheetState.collapse()
            }
        }
    }

    GameDaySudsTheme(selectedTeam.colors) {
        BottomSheetScaffold(
            modifier = Modifier.fillMaxSize(),
            scaffoldState = bottomSheetScaffoldState,
            sheetContent = {
                SheetContent(
                    onSheetClick = sheetToggle
                ) {
                    BeerSheetContent(beer = viewModel.selectedBeer)
                }
            },
            sheetPeekHeight = 0.dp,
            topBar = {
                TopAppBar(
                    title = {Text("Select A Beer To Learn More")},
                    backgroundColor = MaterialTheme.colors.primary,
                    contentColor = Color.White
                )
            }
        ) {
            Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background) {
                SudsList(
                    onClickBeer = onClickBeer,
                    sudsList = viewModel.sudsListResponse,
                    sheetToggle
                )
            }
        }
    }
}

@Composable
fun SudsList(
    onClickBeer: (Int) -> Unit = {},
    sudsList: List<Beer>,
    sheetToggle: () -> Unit,
) {
    Column(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        LazyColumn(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            itemsIndexed(items = sudsList) { _, item ->
                SudsListItem(
                    onClickBeer,
                    beer = item,
                    sheetToggle
                )
            }
        }
    }
}

@Composable
fun SudsListItem(
    onClickBeer: (Int) -> Unit,
    beer: Beer,
    sheetToggle: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .clickable {
                onClickBeer(beer.id)
                sheetToggle()
            }
            .fillMaxWidth()
            .background(color = Color.LightGray)
    ) {
        Text(
            text = beer.name,
            modifier = Modifier
                .padding(16.dp)
        )
    }
    Divider(color = MaterialTheme.colors.primary, thickness = 4.dp)
    Divider(color = MaterialTheme.colors.secondary, thickness = 4.dp)
}