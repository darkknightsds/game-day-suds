package com.darkknightsds.gamedaysuds.suds

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text

import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun SheetContent(
    onSheetClick: () -> Unit,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .clickable {
                onSheetClick()
            }
    ) {
        content()
    }
}


@Composable
fun BeerSheetContent(
    beer: Beer
) {
    Column(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
            .padding(16.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Center
    ) {
        Divider(color = MaterialTheme.colors.primary, thickness = 4.dp)
        Divider(color = MaterialTheme.colors.secondary, thickness = 4.dp)
        Text("Name: ${beer.name}")
        Text("ABV: ${beer.abv}")
        Text("IBU: ${beer.ibu}")
        Text("Food Pairings: ${beer.food_pairing.joinToString(", ")}")
        Divider(color = MaterialTheme.colors.secondary, thickness = 4.dp)
        Divider(color = MaterialTheme.colors.primary, thickness = 4.dp)
    }
}

