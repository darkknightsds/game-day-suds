package com.darkknightsds.gamedaysuds.suds

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface SudsService {
    @GET("beers")
    suspend fun getSuds() : List<Beer>

    companion object {
        var sudsService: SudsService? = null
        fun getInstance() : SudsService {
            if (sudsService == null) {
                sudsService = Retrofit.Builder()
                    .baseUrl("https://api.punkapi.com/v2/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(SudsService::class.java)
            }
            return sudsService!!
        }
    }
}