package com.darkknightsds.gamedaysuds.suds

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class SudsViewModel: ViewModel() {
    var sudsListResponse: List<Beer> by mutableStateOf(listOf())
    var errorMessage: String by mutableStateOf("")
    var selectedBeer: Beer by mutableStateOf(
        Beer(
            0,
            "",
            0f,
            0f,
            listOf("")
        )
    )

    fun getSuds() {
        viewModelScope.launch {
            val sudsService = SudsService.getInstance()
            try {
                val sudsList = sudsService.getSuds()
                sudsListResponse = sudsList
            }
            catch (e: Exception) {
                errorMessage = e.message.toString()
            }
        }
    }
}