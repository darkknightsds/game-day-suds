package com.darkknightsds.gamedaysuds.ui.theme

import androidx.compose.ui.graphics.Color

class ColorItem(val name: String, val color: Color)

val colorList = listOf(
    ColorItem(
        "Red",
        Color(0xFFc62828)
    ),
    ColorItem(
        "Black",
        Color(0xFF000000)
    ),
    ColorItem(
        "DarkBlue",
        Color(0xFF1a237e)
    ),
    ColorItem(
        "Orange",
        Color(0xFFff5722)
    ),
    ColorItem(
        "RoyalBlue",
        Color(0xff1565c0)
    ),
    ColorItem(
        "White",
        Color(0xffffffff)
    ),
    ColorItem(
        "DarkOrange",
        Color(0xffd84315)
    ),
    ColorItem(
        "CoolBlue",
        Color(0xff42a5f5)
    ),
    ColorItem(
        "Yellow",
        Color(0xffffea00)
    ),
    ColorItem(
        "DarkRed",
        Color(0xff9a0007)
    ),
    ColorItem(
        "KellyGreen",
        Color(0xff43a047)
    ),
    ColorItem(
        "Brown",
        Color(0xff5d4037)
    ),
    ColorItem(
        "Teal",
        Color(0xff26a69a)
    ),
    ColorItem(
        "Purple",
        Color(0xFF6200EE)
    )
)