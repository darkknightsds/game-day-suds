package com.darkknightsds.gamedaysuds.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.darkknightsds.gamedaysuds.selectTeam.Colors

@Composable
fun DefaultTheme(content: @Composable () -> Unit) {
    val colors = lightColors(
        primary = getColor(colorList, "RoyalBlue"),
        secondary = getColor(colorList, "Red")
    )

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

@Composable
fun GameDaySudsTheme(teamColors: Colors, content: @Composable () -> Unit) {
    val colors = lightColors(
        primary = getColor(colorList, teamColors.primary),
        secondary = getColor(colorList, teamColors.secondary)
    )

    MaterialTheme(
            colors = colors,
            typography = Typography,
            shapes = Shapes,
            content = content
    )
}

fun getColor(list: List<ColorItem>, colorName: String): Color {
    return list.filter { it.name == colorName }[0].color
}