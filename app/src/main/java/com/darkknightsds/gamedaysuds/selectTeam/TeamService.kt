package com.darkknightsds.gamedaysuds.selectTeam

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface TeamService {
    @GET("teams.json")
    suspend fun getTeams() : List<Team>

    companion object {
        var teamService: TeamService? = null
        fun getInstance() : TeamService {
            if (teamService == null) {
                teamService = Retrofit.Builder()
                    .baseUrl("https://gitlab.com/darkknightsds/teams-json/-/raw/main/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(TeamService::class.java)
            }
            return teamService!!
        }
    }
}