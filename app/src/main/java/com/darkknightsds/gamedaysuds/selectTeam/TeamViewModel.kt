package com.darkknightsds.gamedaysuds.selectTeam

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class TeamViewModel : ViewModel() {
    var teamListResponse: List<Team> by mutableStateOf(listOf())
    var errorMessage: String by mutableStateOf("")
    var selectedTeam: Team by mutableStateOf(
        Team(
            "",
            Colors(
                "",
                ""
            )
        )
    )

    fun getTeams() {
        viewModelScope.launch {
            val teamService = TeamService.getInstance()
            try {
                val teamList = teamService.getTeams()
                teamListResponse = teamList.sortedBy { it.name }
            }
            catch (e: Exception) {
                errorMessage = e.message.toString()
            }
        }
    }
}