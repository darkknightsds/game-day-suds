package com.darkknightsds.gamedaysuds.selectTeam

data class Team(val name: String, val colors: Colors)

data class Colors(val primary: String, val secondary: String)