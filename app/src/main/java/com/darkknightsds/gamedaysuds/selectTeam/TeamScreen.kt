package com.darkknightsds.gamedaysuds.selectTeam

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.darkknightsds.gamedaysuds.ui.theme.DefaultTheme

@Composable
fun TeamBody(
    onClickFavoriteTeam: (Team) -> Unit = {},
    viewModel: TeamViewModel
) {
    viewModel.getTeams()
    DefaultTheme {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = {Text("Select Your Favorite Team")},
                    backgroundColor = MaterialTheme.colors.primary,
                    contentColor = Color.White
                )
                     },
            modifier = Modifier.fillMaxSize()
        ) {
            TeamList(
                onClickFavoriteTeam = onClickFavoriteTeam,
                teamList = viewModel.teamListResponse
            )
        }
    }
}

@Composable
fun TeamList(
    onClickFavoriteTeam: (Team) -> Unit,
    teamList: List<Team>
) {
    Column(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        LazyColumn(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            itemsIndexed(items = teamList) { _, item ->
                TeamListItem(
                    onClickFavoriteTeam = onClickFavoriteTeam,
                    team = item
                )
            }
        }
    }
}

@Composable
fun TeamListItem(
    onClickFavoriteTeam: (Team) -> Unit,
    team: Team
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier.clickable {
            onClickFavoriteTeam(team)
        }
    ) {
        Text(
            text = team.name,
            modifier = Modifier
                .padding(16.dp)
        )
    }
    Divider(color = Color.Gray, thickness = 1.dp)
}